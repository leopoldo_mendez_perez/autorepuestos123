<?php
/**
 * Plugin Name: Parallax Section
 * Description: Makes background element scrolls slower than foreground content.
 * Version: 1.0.1
 * Author: bPlugins LLC
 * Author URI: http://bplugins.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain: parallax-section
 */

// ABS PATH
if ( !defined( 'ABSPATH' ) ) { exit; }

// Constant
define( 'PSB_PLUGIN_VERSION', 'localhost' === $_SERVER['HTTP_HOST'] ? time() : '1.0.1' );
define( 'PSB_ASSETS_DIR', plugin_dir_url( __FILE__ ) . 'assets/' );

// Generate Styles
class PSBStyleGenerator {
	public static $styles = [];
	public static function addStyle( $selector, $styles ){
		if( array_key_exists( $selector, self::$styles ) ){
			self::$styles[$selector] = wp_parse_args( self::$styles[$selector], $styles );
		}else { self::$styles[$selector] = $styles; }
	}
	public static function renderStyle(){
		$output = '';
		foreach( self::$styles as $selector => $style ){
			$new = '';
			foreach( $style as $property => $value ){
				if( $value == '' ){ $new .= $property; }else { $new .= " $property: $value;"; }
			}
			$output .= "$selector { $new }";
		}
		return $output;
	}
}

// Parallax Section
class PSBParallaxSection{
	function __construct(){
		add_action( 'init', [$this, 'onInit'] );
	}

	function onInit() {
		wp_register_style( 'psb-parallax-editor-style', plugins_url( 'dist/editor.css', __FILE__ ), [ 'wp-edit-blocks' ], PSB_PLUGIN_VERSION ); // Backend Style
		wp_register_style( 'psb-parallax-style', plugins_url( 'dist/style.css', __FILE__ ), [ 'wp-editor' ], PSB_PLUGIN_VERSION ); // Both Style

		register_block_type( __DIR__, [
			'editor_style'		=> 'psb-parallax-editor-style',
			'style'				=> 'psb-parallax-style',
			'render_callback'	=> [$this, 'render']
		] ); // Register Block

		wp_set_script_translations( 'psb-parallax-editor-script', 'parallax-section', plugin_dir_path( __FILE__ ) . 'languages' ); // Translate
	}

	function render( $attributes, $content ){
		extract( $attributes );

		$psbParallaxSectionStyle = new PSBStyleGenerator(); // Generate Styles
		$psbParallaxSectionStyle::addStyle( "#psbParallaxSection-$cId", [
			'min-height' => $minHeight
		] );
		$psbParallaxSectionStyle::addStyle( "#psbParallaxSection-$cId .psbParallaxSection", [
			'justify-content' => $verticalAlign,
			'text-align' => $textAlign,
			'min-height' => $minHeight,
			'padding' => $padding['styles'] ?? '20px 30px'
		] );
		$psbParallaxSectionStyle::addStyle( "#psbParallaxSection-$cId .psbParallaxImg", [
			$background['styles'] => ''
		] );

		ob_start(); ?>
		<div class='wp-block-psb-parallax <?php echo 'align' . esc_attr( $align ); ?>' id='psbParallaxSection-<?php echo esc_attr( $cId ) ?>'>
			<style><?php echo wp_kses( $psbParallaxSectionStyle::renderStyle(), [] ); ?></style>

			<div class='psbParallaxImg' data-speed='<?php echo esc_attr( $speed ); ?>'></div>

			<div class='psbParallaxSection'>
				<?php echo wp_kses_post( $content ); ?>
			</div> <!-- Parallax Section -->
		</div>

		<?php $psbParallaxSectionStyle::$styles = []; // Empty styles
		return ob_get_clean();
	} // Render
}
new PSBParallaxSection;