<?php

// Don't redefine the functions if included multiple times.
if (!function_exists('RexGuzzleHttp\uri_template')) {
    require __DIR__ . '/functions.php';
}
