<?php

namespace RexGuzzleHttp\Exception;

final class InvalidArgumentException extends \InvalidArgumentException implements GuzzleException
{
}
