<?php
namespace RexGuzzleHttp\Exception;

class TooManyRedirectsException extends RequestException
{
}
