<?php
namespace RexGuzzleHttp\Exception;

class TransferException extends \RuntimeException implements GuzzleException
{
}
