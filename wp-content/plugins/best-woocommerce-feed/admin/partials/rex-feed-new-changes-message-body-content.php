<div  id="rex_feed_new_changes_msg_content">
    <div class="new_changes_msg_contnent">
        <div class="new_changes_msg_heading">
            <h2><?php _e( 'Promising new user interaface of the Product Feed Manager plugin - <strong>Changes you may want to know</strong>.', 'rex-product-feed' ); ?></h2>
            <p><span style="color: red">*</span>Please clean your browser cache for improved performance.</p>
        </div>
    </div>
    <div id="rex_feed_new_changes_msg_btn">
        <a id="view_changes_btn" href="https://rextheme.com/new-interface-of-product-feed-manager/" target="_blank">
            <?php _e( 'View Changes', 'rex-product-feed' ) ?></a>
    </div>
</div>