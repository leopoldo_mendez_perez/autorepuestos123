<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines all the Metaboxes for Products
 *
 * @package    Rex_Product_Metabox
 * @subpackage Rex_Product_Feed/admin
 * @author     RexTheme <info@rextheme.com>
 */
class Rex_Product_Metabox
{
	private $prefix = 'rex_feed_';


	/**
	 * Register all metaboxes.
	 *
	 * @since    1.0.0
	 */
	public function register()
	{
		$is_premium = apply_filters( 'wpfm_is_premium_activate', false );

		add_action( 'add_meta_boxes', array( $this, 'rex_feed_filter_settings_section' ) );
		add_action( 'add_meta_boxes', array( $this, 'rex_feed_feed_config_section' ) );
		add_action( 'add_meta_boxes', array( $this, 'rex_feed_product_settings_section' ) );
		add_action( 'add_meta_boxes', array( $this, 'rex_feed_product_filters_section' ) );
		add_action( 'add_meta_boxes', array( $this, 'rex_feed_feed_file_section' ) );

		$post_id   = isset( $_GET[ 'post' ] ) ? $_GET[ 'post' ] : '';
		$post_type = $post_id !== '' ? get_post_type( $post_id ) : '';

		if ( $post_type === 'product-feed' ) {
			$this->rex_feed_trigger_based_review_helper();
        }

        if ( $post_type === '' && isset( $_GET[ 'post_type' ] ) ) {
            $post_type = $_GET[ 'post_type' ];
        }
        if ( $post_type === 'product-feed' ) {
            $this->rex_feed_new_changes_message();
        }

		if ( $this->rex_feed_is_google_merchant() ) {
			add_action( 'add_meta_boxes', array( $this, 'rex_feed_google_merchant_section' ) );
		}
		if ( !$is_premium ) {
			add_action( 'add_meta_boxes', array( $this, 'rex_feed_upgrade_notice_section' ) );
		}
        add_action( 'add_meta_boxes', array( $this, 'rex_feed_feed_how_to_guide_section' ) );
	}


	/**
	 * Check if current merchant is google
	 */
	private function rex_feed_is_google_merchant()
	{
		$feed_id = isset( $_GET['post'] ) ? $_GET['post'] : '';
		$merchant = get_post_meta( $feed_id, 'rex_feed_merchant', true );
		return 'google' === $merchant;
	}


	/**
	 * Adding metabox for Filter & Settings button section
	 */
	public function rex_feed_filter_settings_section()
	{
		add_meta_box(
			$this->prefix . 'head_btn',
			'Add New Feed',
			array( $this, 'rex_feed_generate_filter_settings_section' ),
			'product-feed',
			'normal',
			'high'
		);
	}


	/**
	 * Generates the Add New Feed Heading
	 * and Additional Filter & Settings Button
	 */
	public function rex_feed_generate_filter_settings_section()
	{
        require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-filter-settings-body-content.php';
	}


	/**
	 * Adding metaboxes for merchant, feed format
	 * and feed separator dropdown list section
	 * & also feed config table section
	 */
	public function rex_feed_feed_config_section()
	{
		add_meta_box(
			$this->prefix . 'conf',
			'Feed Configuration',
			array( $this, 'rex_feed_generate_merchant_dropdown_section' ),
			'product-feed',
			'normal',
			'core'
		);
		add_meta_box(
			$this->prefix . 'progress_bar',
			'Progress Bar',
			array( $this, 'progress_config_cb' ),
			'product-feed',
			'normal',
			'core'
		);
		add_meta_box(
			$this->prefix . 'config_heading',
			'Configure Feed Attributes and their values',
			array( $this, 'rex_feed_generate_config_table' ),
			'product-feed',
			'normal',
			'core'
		);
	}


	/**
	 * Generates the feed merchant, feed format and separator dropdown lists section
	 */
	public function rex_feed_generate_merchant_dropdown_section()
	{
		$saved_merchant   = get_post_meta( get_the_ID(), 'rex_feed_merchant', true );
		$file_format      = get_post_meta( get_the_ID(), 'rex_feed_feed_format', true );

		require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-merchant-dropdown-section.php';

		if ( wpfm_pro_compatibility() ) {
			do_action( 'wpfm_merchant_settings_fields', $this->prefix );
        }
	}


	/**
	 * Generates the feed config table section
	 */
	public function rex_feed_generate_config_table()
	{
		?>
        <table id="config-table" class="responsive-table wpfm-field-mappings">
			<?php require_once plugin_dir_path( __FILE__ ) . 'partials/loading-spinner.php';?>
        </table>

        <div id="rex-feed-footer-btn" class="rex-feed-footer-btn">
            <a id="rex-new-attr" class="rex-new-custom-btn">
		        <?php include WPFM_PLUGIN_ASSETS_FOLDER_PATH . 'icon/icon-svg/icon-plus.php';?>
		        <?php echo __( 'Add New Attribute', 'rex-product-feed' ) ?>
            </a>
            <a id="rex-new-custom-attr" class="rex-new-custom-btn">
				<?php include WPFM_PLUGIN_ASSETS_FOLDER_PATH . 'icon/icon-svg/icon-plus.php';?>
				<?php echo __( 'Add New Custom Attribute', 'rex-product-feed' ) ?>
            </a>
        </div>

		
		<?php
	}


	/**
	 * Adding metaboxes for product settings section
	 */
	public function rex_feed_product_settings_section()
	{
		add_meta_box(
			$this->prefix . 'product_settings',
			__('Settings', 'rex-product-feed'),
			array( $this, 'rex_feed_generates_product_settings_section' ),
			'product-feed',
			'normal',
			'core'
		);
	}


	/**
	 * Generates the product settings section
	 */
	public function rex_feed_generates_product_settings_section()
	{
		$schedules = apply_filters(
			'wpfm_option_schedules', array(
			'no'     => __( 'No Interval', 'rex-product-feed' ),
			'hourly' => __( 'Hourly', 'rex-product-feed' ),
			'daily'  => __( 'Daily', 'rex-product-feed' ),
			'weekly' => __( 'Weekly', 'rex-product-feed' ),
			'custom' => __( 'Custom', 'rex-product-feed' ),
		) );
		require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-product-settings-section.php';
	}


	/**
	 * Adding metaboxes for product filters section
	 */
	public function rex_feed_product_filters_section()
	{
		add_meta_box(
			$this->prefix . 'product_filters',
			'Filters',
			array( $this, 'rex_feed_generates_product_filters_section' ),
			'product-feed',
			'normal',
			'core'
		);
	}


	/**
	 * Generates the product filters section
	 */
	public function rex_feed_generates_product_filters_section()
	{
		$options = array(
			'all'            => __( 'All Published Products', 'rex-product-feed' ),
			'featured'       => __( 'All Featured Products', 'rex-product-feed' ),
			'filter'         => __( 'Custom Filters', 'rex-product-feed' ),
			'product_cat'    => __( 'Category Filters', 'rex-product-feed' ),
			'product_tag'    => __( 'Tag Filters', 'rex-product-feed' )
		);

		if ( wpfm_pro_compatibility() ) {
			$options = apply_filters( 'wpfm_product_filter_options', $options );
		}

		require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-product-filter-header-section.php';
        // rex-contnet-filter__header end

		require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-filter-products-dropdown-section.php';
        // rex-content-filter__area end

		$this->rex_feed_custom_filter_section();
		$this->rex_feed_product_taxonomies();

        if ( wpfm_pro_compatibility() ) {
	        do_action( 'wpfm_product_filter_fields', $this->prefix );
        }
	}


	/**
	 * Generates custom filters in product filter section
	 **/
	public function rex_feed_custom_filter_section()
	{
		$feed_filter_rules = get_post_meta( get_the_ID(), $this->prefix . 'feed_config_filter', true );
		$feed_filter       = new Rex_Product_Filter( $feed_filter_rules );
		?>
        <div id="rex-feed-config-filter" class="rex-feed-config-filter">
			<?php require_once plugin_dir_path( __FILE__ ) . 'partials/loading-spinner.php'; ?>
			<?php require_once plugin_dir_path( __FILE__ ) . 'partials/feed-config-metabox-display-filter.php'; ?>
            <a id="rex-new-filter" class="rex-new-custom-btn">
				<?php include WPFM_PLUGIN_ASSETS_FOLDER_PATH . 'icon/icon-svg/icon-dark-plus.php';?>
				<?php echo __( 'Add New Filter', 'rex-product-feed' ) ?>
            </a>
        </div>
		<?php
	}


	/**
	 * Generates product categories and tags in product filter section
	 **/
	public function rex_feed_product_taxonomies()
	{
		echo '<div class="rex-feed-product-taxonomies-spinner" style="display: none; "><img src="' . WPFM_PLUGIN_ASSETS_FOLDER . 'icon/loader.gif" alt="spinner" /></div>';
		echo '<div id="rex-feed-product-taxonomies" class="rex-feed-product-taxonomies">';
		echo '</div>';
	}


	/**
	 * Adding metaboxes for feed file section
	 */
	public function rex_feed_feed_file_section()
	{
		$feed_url = get_post_meta( get_the_ID(), $this->prefix . 'xml_file', true );

		if ( strlen( $feed_url ) > 0 ) {
			add_meta_box(
				$this->prefix . 'file_link',
				'Feed URL',
				array( $this, 'rex_feed_generate_feed_file_section' ),
				'product-feed',
				'side',
				'core'
			);
		}
	}


	/**
	 * Adding metaboxes for How To Guide Section
	 */
	public function rex_feed_feed_how_to_guide_section()
	{
        add_meta_box(
            $this->prefix . 'how_to_guide',
            'Common Troubleshooting Issue',
            array( $this, 'rex_feed_generate_how_to_guide_section' ),
            'product-feed',
            'side',
            'core'
        );
	}


	/**
	 * Generates the feed file section
	 */
	public function rex_feed_generate_feed_file_section()
	{
		$feed_url = get_post_meta( get_the_ID(), $this->prefix . 'xml_file', true );
		$feed_url = esc_url( $feed_url );

        require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-feed-file-section-content.php';
	}


	/**
	 * Generates the How To Guide section
	 */
	public function rex_feed_generate_how_to_guide_section()
	{
        require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-how-to-guide-content.php';
	}


	/**
	 * Helper function to decide if review request
	 * metabox needs to be generated.
	 **/
	private function rex_feed_trigger_based_review_helper()
	{
		$show_review_request = get_option( 'rex_feed_review_request' );

		if ( ! empty( $show_review_request ) && isset( $show_review_request[ 'show' ] ) && $show_review_request[ 'show' ] ) {

			if ( isset( $show_review_request[ 'frequency' ] ) ) {
				if ( $show_review_request[ 'frequency' ] == 'immediate' ) {
					add_action( 'admin_notices', array( $this, 'rex_feed_generate_review_request_section' ) );
				}
                elseif ( $show_review_request[ 'frequency' ] == 'one_week' ) {
					$last_shown_date = $show_review_request[ 'time' ];
					$current_date    = time();
					$current_date    = new DateTime( date( 'Y-m-d', $current_date ) );
					$last_shown_date = new DateTime( date( 'Y-m-d', $last_shown_date ) );
					$date_diff       = $last_shown_date->diff( $current_date );

					if ( $date_diff->d > 7 ) {
						add_action( 'admin_notices', array( $this, 'rex_feed_generate_review_request_section' ) );
					}
				}
			}
		}
	}


    /**
     * Generates body contents for trigger based review request section
     **/
    public function rex_feed_generate_review_request_section()
    {
        require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-review-request-body-content.php';
    }


	/**
	 * Helper function to decide if changes message should be displayed or not
	 **/
	private function rex_feed_new_changes_message()
	{
		$show = get_option( 'rex_feed_new_changes_msg' );

		if ( empty( $show ) ) {
            add_action( 'admin_notices', array( $this, 'rex_feed_generate_new_changes_message' ) );
		}
	}


	/**
	 * Generates body contents for new changes message section
	 **/
	public function rex_feed_generate_new_changes_message()
	{
		require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-new-changes-message-body-content.php';
	}


	/**
	 * Adding metaboxe for google merchant section
	 */
	public function rex_feed_google_merchant_section() {
		add_meta_box(
			$this->prefix . 'google_merchant',
			esc_html__( 'Send to Google Merchant', 'rex-product-feed' ),
			array( $this, 'rex_feed_generate_google_merchant_section' ),
			'product-feed',
			'side',
			'core'
		);
	}


	/**
	 * Generates google merchant section
	 **/
	public function rex_feed_generate_google_merchant_section()
	{
		echo '<h2>' . __( 'Send to Google Merchant', 'rex-product-feed' ) . '</h2>';
		$this->rex_feed_google_merchant_desc();
		$destinations = array(
			'Display Ads'      => __( 'Display Ads', 'rex-product-feed' ),
			'Shopping Ads'     => __( 'Shopping Ads', 'rex-product-feed' ),
			'Shopping Actions' => __( 'Shopping Actions', 'rex-product-feed' ),
		);

		$schedules = array(
			'monthly' => __( 'Monthly', 'rex-product-feed' ),
			'weekly'  => __( 'Weekly', 'rex-product-feed' ),
			'hourly'  => __( 'Hourly', 'rex-product-feed' ),
		);

		$month_array = range( 1, 31 );
		array_unshift( $month_array, "" );
		unset( $month_array[ 0 ] );

		$weeks = array(
			'monday'    => 'Monday',
			'tuesday'   => 'Tuesday',
			'wednesday' => 'Wednesday',
			'thursday'  => 'Thursday',
			'friday'    => 'Friday',
			'saturday'  => 'Saturday',
			'sunday'    => 'Sunday',
		);

		require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-google-merchant.php';

		$this->rex_feed_after_field_google_merchant();
	}


	/**
	 * Output a message if the feed merchant is google
	 */
	public function rex_feed_after_field_google_merchant()
	{
		$feed_merchant = get_post_meta( get_the_ID(), 'rex_feed_merchant', true );

		if ( $feed_merchant === 'google' ) {
			$rex_google_merchant = new Rex_Google_Merchant_Settings_Api();
			$message             = __( 'Oops!! Access token has expired 😕 Please authenticate token for Google Merchant Shop to be able to send feed.', 'rex-product-feed' );
			if ( !( $rex_google_merchant->is_authenticate() ) ) {
				echo sprintf(
					'<p class="google-status">%s <a href="%s">' . __( 'Authenticate', 'rex-product-feed' ) . '</a> </p>',
					$message,
					admin_url( 'admin.php?page=merchant_settings' ) );
			}
			else {
				echo '<a class="btn waves-effect waves-light" id="send-to-google" href="#">
                        ' . __( 'Send to google merchant', 'rex-product-feed' ) . '
                      </a> ';
			}
			echo '<div class="rex-google-status"></div>';
		}
	}


	/**
	 * Output a message if the current page has the id of "2" (the about page)
	 */
	public function rex_feed_google_merchant_desc()
	{
		echo sprintf( __( '<p class="google-desc">Please note that Google has fixed abbreviations for Location and Language. For example, the abbreviation for target location, United States is US and the abbreviation for language, English is en. <a href="https://rextheme.com/google-country-codes-list/" target="_blank">Click here</a> to see the list of all abbreviations set by Google.</p>', 'rex-product-feed' ) );
	}


	/**
	 * Adding metaboxe for upgrade notice for pro section
	 */
	public function rex_feed_upgrade_notice_section()
	{
		add_meta_box(
			$this->prefix . 'upgrade_notice',
			esc_html__( 'Upgrade Notice', 'rex-product-feed' ),
			array( $this, 'rex_feed_generate_upgrade_notice_section' ),
			'product-feed',
			'side',
			'core'
		);
	}


	/**
	 * Generates upgrade notice for pro section
	 **/
	public function rex_feed_generate_upgrade_notice_section()
	{
		require_once plugin_dir_path( __FILE__ ) . 'partials/rex-feed-upgrade-to-pro-notice-section.php';
	}

//  ==================================================

	/**
	 * Display Feed Config Metabox.
	 *
	 * @return void
	 * @author RexTheme
	 **/
	public function progress_config_cb()
	{

		echo '<div id="rex-feed-progress" class="rex-feed-progress">';
		require_once plugin_dir_path( __FILE__ ) . 'partials/progress-bar.php';
		echo '</div>';
	}
}